﻿// ConsoleAppWinAPI.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
// FAQ: Для корректной работы программы, требуется чтобы консольное окно было размером 100 колонн на 30 строк.

#include <iostream>
#include <Windows.h>
#include <string>
#include <stdio.h> // Что то в программе и Дейкстры
#include <fstream>
#include "LibraryForApp.h"
#include <sstream>
#include <iomanip> // Для форматированного вывода, манипулятор setw(2);
#include <stdlib.h> // Дейкстры

#define AdressFile "C:\Users\Barclay\Desktop\Matrix.txt"

using namespace std;

//Функция загрузки программы, анимаций загрузки программы. 
void StartAnimation(PageStatus& PageStat);
//Страницы главного меню.
void PageMainMenuOne(PageStatus& PageStat); // Кнопка пункта Description
void PageMainMenuTwo(PageStatus& PageStat); // Кнопка пункта Graph
void PageMainMenuThree(PageStatus& PageStat); // Кнопка пункта Settings
// Страницы после главного меню.
void PageDescription(PageStatus& PageStat); // Страница Description
void PageGraph(PageStatus& PageStat); // Страница Graph
void PageSettings(PageStatus& PageStat); // Страница Settings
// Возврат индекса символа 
int ReturnSymbolsAfterNombers(byte& InputSymbol);
// Возврат символа по индексу.
char ReturnNumbersAfteSymbols(int& InputNumber);
// Алгоритм Дейкстры
void DekstrsAlgorithm(int** Matrix, int& SizeMatrix, int& IndexStart, int& IndexEnd);


int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    PageStatus StatPageProgram = ProgramStart;
    const int Row = 60;
    const int Columns = 200;
    // Получаю дескриптор консольного окна. 
    HANDLE OriginalDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    // Настрайваю цветовую схему фона и символов.
    SetConsoleTextAttribute(OriginalDescriptor, BlueWithWhiteCLS); system("CLS");
    // Далее 2 строки для настройки консольного окна которые у меня не работают.
    SMALL_RECT OutputWindowSize = {0,0,Columns - 1,Row - 1};
    SetConsoleWindowInfo(OriginalDescriptor, TRUE, &OutputWindowSize);
    //***************************************************************************************************************************
    //
    // Переключи, раскомментируй StartAnimation(StatPageProgram); и наоборот StatPageProgram = MainMenuOnePosition;
    // Выбери что то одно, другое закомментируй....
    //***************************************************************************************************************************
    StartAnimation(StatPageProgram);
    //StatPageProgram = MainMenuOnePosition;

    while (true) {
        switch (StatPageProgram)
        {   
       /* case ProgramStart:
            break;
        case QProgramStartAnimation:
            break;*/
        case MainMenuOnePosition:
            PageMainMenuOne(StatPageProgram);
            break;
        case MainMenuTwoPosition:
            PageMainMenuTwo(StatPageProgram);
            break;
        case MainMenuThreePosition:
            PageMainMenuThree(StatPageProgram);
            break;
        case StatPageDescription:
            PageDescription(StatPageProgram);
            break;
        case StatPageGraph:
            PageGraph(StatPageProgram);
            break;
        case StatPageSettings:
            PageSettings(StatPageProgram);
            break;



        case ProgramExit:
            exit(0);
            break;
        default:
            break;
        }
    }
}

void PageSettings(PageStatus& PageStat){ // Страница Settings
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Settings
    */
    // Начало кнопки Settings, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 12;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 31;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "############# Settings #############";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,12,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    CursorPosition = { 1,7 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " Данный раздел находится в разработке. ";

    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);//  
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  \/
    // Если на данный момент работает эта функция, то StatPageProgram = StatPageSettings = 7;
    // Тут мы устанавливаем статус переменной PageStat.
    while (true) {
        // Если нажали enter или стрелку вправо, то выполняются какие либо действия на странице меню. 
        if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {
            CursorPosition = { 50,20 };
            SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
            cout << "Settings";//  
            // Для каких либо действий на странице.
        }
        // Если нажали ESCAPE или стрелку влево, то выходим в главное меню где подсвечена Settings
        if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
            PageStat = MainMenuThreePosition;
            break;
        }
        // На данной странице эти кнопки не нужны.
        //// Если нажали вверх, то переходим на страницу PageMainMenuThree; 
        //if (GetAsyncKeyState(VK_UP) == -32767) {
        //    PageStat = MainMenuThreePosition;
        //    break;
        //}
        //// Если нажали вниз, то переходим на страницу PageMainMenuTwo; 
        //if (GetAsyncKeyState(VK_DOWN) == -32767) {
        //    PageStat = MainMenuTwoPosition;
        //    break;
        //}

    }
}

void PageGraph(PageStatus& PageStat) { // Страница Graph
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    string PathFile; char InputDataFileBufer;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Graph
    */
    // Начало кнопки Graph, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 9;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 31;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "########## Graph ##########";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,9,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    CursorPosition = { 1,7 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " Данный раздел находится в разработке. ";
    */

    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);//  
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  Вывод символов \/

    /*
    Интерфейс Graph
    */

    //CursorPosition = { 21,4 }; SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    CursorPosition = { 1 ,7 }; SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "Введите путь к файлу с данными...\n  ";
    CursorPosition = { 1, 8 };  SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    //cout << "\t\t";

    // Где то тут надо создать  функцию куда передаем путь к файлу, а она возвращает указатель на массив с элементами.
    cin >> PathFile;
    ////getline(cin, Path);


    // Тут надо убавить количество миллисекунд так чтобы не срабатывала клавиша Enter дальше. 
    Sleep(500);
    ifstream OriginalFile(PathFile);
    // Объявление переменных для считывания данных из файла для получения размерности матрицы.
    string data; byte SizeArray = 0; byte Check, CheckTwo;
    __int8 i, j;
    int FromFileToArray;
    if (!OriginalFile.is_open()) // если файл не открыт
        cout << "\n Файл не может быть открыт!"; // сообщить об этом
    // Придумать что сделать если файл, не откроется.                                                                
    else {
        cout << "\n Файл был успешно открыт";
        Sleep(500);
        CursorPosition = { 1, 11 };  SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "                        ";
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    }
    getline(OriginalFile, data);
    // Обработка данных со строки:
    // Тут мы считывая первую строку в файле, получаем количество вершин.
    istringstream ss(data);
    ss >> Check;
    SizeArray++;
    CheckTwo = Check;
    while (INFINITY) {
        ss >> Check;
        if (CheckTwo == Check) {
            break;
        }
        else CheckTwo = Check;
        SizeArray++;
    }
    // Теперь мы имеем количество вершин SizeArray , следовательно размерность нашего массива. 
    // static_cast<int>(SizeArray) это вывод реального числа а не символа....
    int Size = SizeArray;
    // Далее создаем динамичесуий массив, нашей матрицы смежности: 
    int** MatrixTable = new int* [Size];
    for (i = 0;i < Size; i++) {
        MatrixTable[i] = new int[Size];
    } i = 0; j = 0;
    // Объявили его, выделили память. Заполняем данными из файла.
    // Далее, увы придется снова считывать построчно а потом дробить эту строку и с нее заполнять(((.
    // Заполняем нашу матрицу смежностей значениями из файла.

    cout << "Количество вершин: " << Size << " \n";
    // Далее тут и считываю поиндексно числа из файла в динамический массив.
    while (i < Size) {
        //OriginalFile >> MatrixTable[i][0];
        //OriginalFile.seekg(5);
        // Всякое пробовал с буквами с первым столбиком, т.е. с первым символом в каждой строке. Не работает.
        while (j < Size) {
            OriginalFile >> MatrixTable[i][j];
            j++;
        }
        j = 0;
        i++;
    }  i = 0; j = 0;

    // Устанавливаю поля вывода чтобы названия вершин не сместились в таблице.
    cout << " Исходная матрица смежности:\n";
    // Вывод массива из файла.
    // Вывод сстроки символов
    i = 65;
    while (i <= 64 + Size) {
        cout << setw(3) << char(i) << "  ";
        i++;
    }
    cout << "\n";
    i = 0;
    // Вывод элементов.
    while (i < Size) {
        while (j < Size) {
            cout << setw(3) << MatrixTable[i][j] << "  ";
            j++;
        }
        cout << "\n";
        j = 0;
        i++;
    }  i = 0; j = 0;

    // Вечный цикл перед выходом...
    while(INFINITY) {
        // Вечный цикл который позволяет если потребовалось ввести дополнительное количество  точек, 
        // то продолжаем по новой вводить...
        // Далее считываем список вершин откуда и куда следует проложить вершины:
        cout << " Введите количество вершин куда вам следует проложить путь: ";
        int SizeInputPeaksForStart;
        cin >> SizeInputPeaksForStart; SizeInputPeaksForStart++; // Инкрементирую для начальной точки.
        cout << "\n Введите вершины от какой и в какие требуется проложить маршрут:";
        // Создаю массив символов в который положу старт и вершины куда проложить путь
        byte* InputPeaksForStart = new byte[SizeInputPeaksForStart];
        while (i < SizeInputPeaksForStart) {
            cin >> InputPeaksForStart[i];
            i++;
        } i = 0;
        // Задержка в 3 секунды перед обновлением страницы.
        //Sleep(3000);
        // Далее надо сделать перепрорисовку страницы где должно быть продолжение.
        // СТРАНИЦА 2
        // СТРАНИЦА 2 
        // СТРАНИЦА 2
        // СТРАНИЦА 2
        // СТРАНИЦА 2
        // СТРАНИЦА 2
        // СТРАНИЦА 2
        // СТРАНИЦА 2
        // СТРАНИЦА 2

        // Зарисовка кнопок.
        CursorPosition = { 49,24 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "  ";//cout << char(47) << char(92);//  
        CursorPosition = { 49,25 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "  ";//cout << "UP";
        CursorPosition = { 9,26 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "       ";//cout << " < BACK";
        CursorPosition = { 84,26 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "       ";//cout << " NEXT >";
        CursorPosition = { 48,27 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "    ";//cout << "DOWN";
        CursorPosition = { 49,28 };
        SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
        cout << "  ";//cout << char(92) << char(47);//  Вывод символов \/
        // Перевод строки для продолжения отображения, обратно.
        CursorPosition = { 1,24 }; SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);

        // АЛГОРИТМ ДЕКСТРЫ

        // Надо создать функцию которая будет принимать символ и возвращать индекс этого символа в алфавите.
        // ВЫВОД ВРШИН В СИМВОЛАХ, Т.Е. КАК МЫ ВИХ И ВВЕЛИ. ДЛЯ РАБОТЫ С АЛГОРИТМОМ ПЕРЕВЕСТИ В ЧИСЛА...
        //ReturnSymbolsAfterNombers(InputPeaksForStart[0]) ВЫВОДИТ ЧИСЛА ИЗ СИВОЛОВ ЧТОБЫ РАБОТАТЬ С АЛГОРИТМОМ.
        cout << "Старт и вершины: ";
        while (i < SizeInputPeaksForStart) {
            cout << InputPeaksForStart[i] << " ";
            i++;
        } i = 0;

        // ТУТ ДАЛЬШЕ ВСЕ РАБОТАЕТ. НАДО РАЗОБРАТЬСЯ С АЛГОРИТМОМ ДЕЙКСТРЫ....
        //ДАЛЬШЕ...
        int Start, end;
        do {
            Start = ReturnSymbolsAfterNombers(InputPeaksForStart[i]);
            end = ReturnSymbolsAfterNombers(InputPeaksForStart[i + 1]);
            DekstrsAlgorithm(MatrixTable, Size, Start, end);
            i++;
        } while (i < SizeInputPeaksForStart - 1);
        Sleep(3000);


        cout << "\nНажмите Enter для продолжения, Escape для выхода в главное меню... ";
        bool exit = false;
        while (INFINITY) {
            // Если нажали enter или стрелку вправо, то выполняются какие либо действия на странице меню. 
            if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {
                break;
            }
            // Если нажали ESCAPE или стрелку влево, то выходим в главное меню где подсвечена Description
            if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
                exit = true;
                break;

            }
        }
        if (exit) {
            PageStat = MainMenuTwoPosition;
            break;
        }
    } 

}

void DekstrsAlgorithm(int** Matrix, int& SizeMatrix, int& IndexStart, int& IndexEnd) {

        // a матрица связей это Matrix
        int *d = new int[SizeMatrix]; // минимальное расстояние
        int *v = new int[SizeMatrix]; // посещенные вершины
        int temp, minindex, min;
        int begin_index = IndexStart;
        //Инициализация вершин и расстояний
        for (int i = 0; i < SizeMatrix; i++)
        {
            d[i] = 10000;
            v[i] = 1;
        }
        d[begin_index] = 0;
        // Шаг алгоритма
        do {
            minindex = 10000;
            min = 10000;
            for (int i = 0; i < SizeMatrix; i++)
            { // Если вершину ещё не обошли и вес меньше min
                if ((v[i] == 1) && (d[i] < min))
                { // Переприсваиваем значения
                    min = d[i];
                    minindex = i;
                }
            }
            // Добавляем найденный минимальный вес
            // к текущему весу вершины
            // и сравниваем с текущим минимальным весом вершины
            if (minindex != 10000)
            {
                for (int i = 0; i < SizeMatrix; i++)
                {
                    if (Matrix[minindex][i] > 0)
                    {
                        temp = min + Matrix[minindex][i];
                        if (temp < d[i])
                        {
                            d[i] = temp;
                        }
                    }
                }
                v[minindex] = 0;
            }
        } while (minindex < 10000);
                //// Вывод кратчайших расстояний до вершин
                //printf("\nКратчайшие расстояния до вершин: \n");
                //for (int i = 0; i < SizeMatrix; i++)
                //    printf("%5d ", d[i]);

        // Восстановление пути
        int *ver = new int[SizeMatrix]; // массив посещенных вершин
        int end = IndexEnd; // индекс конечной вершины = 5 - 1
        ver[0] = end + 1; // начальный элемент - конечная вершина
        int k = 1; // индекс предыдущей вершины
        int weight = d[end]; // вес конечной вершины

        while (end != begin_index) // пока не дошли до начальной вершины
        {
            for (int i = 0; i < SizeMatrix; i++) // просматриваем все вершины
                if (Matrix[i][end] != 0)   // если связь есть
                {
                    int temp = weight - Matrix[i][end]; // определяем вес пути из предыдущей вершины
                    if (temp == d[i]) // если вес совпал с рассчитанным
                    {                 // значит из этой вершины и был переход
                        weight = temp; // сохраняем новый вес
                        end = i;       // сохраняем предыдущую вершину
                        ver[k] = i + 1; // и записываем ее в массив
                        k++;
                    }
                }
        }
        //Вывод пути(начальная вершина оказалась в конце массива из k элементов)
        cout << "\n Вывод кратчайшего пути:";
        for (int i = k - 1; i >= 0; i--)
            cout << ' ' << ReturnNumbersAfteSymbols(ver[i]);
        delete ver; delete d; delete v;
}


char ReturnNumbersAfteSymbols(int& InputNumber) {
    unsigned __int8 ItCast;
    ItCast = InputNumber + 64;
    return ItCast;
}

int ReturnSymbolsAfterNombers(byte& InputSymbol) {
    // 65 это код ASCII первой буквы английского алфавита  
    unsigned __int8 ItCast;
    ItCast = static_cast<int>(InputSymbol) - 65;
    return static_cast<int>(ItCast);
}

void PageDescription(PageStatus& PageStat) { // Страница Description
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Description
    */
    // Начало кнопки Description, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 15;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 31;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "################ Description ################";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,15,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    CursorPosition = { 1,7 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " Данный раздел находится в разработке. ";

    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);//  
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  \/
    // Если на данный момент работает эта функция, то StatPageProgram = StatPageDescription = 5;
    // Тут мы устанавливаем статус переменной PageStat.
    while (true) {
        // Если нажали enter или стрелку вправо, то выполняются какие либо действия на странице меню. 
        if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {
            CursorPosition = { 50,20 };
            SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
            cout << "Description";//  
             // Переход на страницу  Description.
        }
        // Если нажали ESCAPE или стрелку влево, то выходим в главное меню где подсвечена Description
        if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
            PageStat = MainMenuOnePosition;
            break;
        }
        // На данной странице эти кнопки не нужны.
        //// Если нажали вверх, то переходим на страницу PageMainMenuThree; 
        //if (GetAsyncKeyState(VK_UP) == -32767) {
        //    PageStat = MainMenuThreePosition;
        //    break;
        //}
        //// Если нажали вниз, то переходим на страницу PageMainMenuTwo; 
        //if (GetAsyncKeyState(VK_DOWN) == -32767) {
        //    PageStat = MainMenuTwoPosition;
        //    break;
        //}

    }


}

void PageMainMenuOne(PageStatus& PageStat) {
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Description
    */
    // Начало кнопки Description, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 15;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 31;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "################ Description ################";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,15,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Вторая кнопка Graph
    */
    ColumnSymbolsButton = 9;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 241;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "########## Graph ##########";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,9,15,11 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Третья кнопка Settings
    */
    ColumnSymbolsButton = 12;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 241;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "############# Settings #############";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,14,12,16 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);
    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);//  
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  \/
    // Если на данный момент работает эта функция, то StatPageProgram = MainMenuOnePosition = 2;
    // Тут мы устанавливаем статус переменной PageStat.
    while (true) {
        // Если нажали enter или стрелку вправо, то переходим на страницу Description 
        if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {  
            PageStat = StatPageDescription;
            break;
             // Переход на страницу  Description.
        }
        // Если нажали ESCAPE или стрелку влево, то выходим из приложения
        if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
            PageStat = ProgramExit;
            break;
        }
        // Если нажали вверх, то переходим на страницу PageMainMenuThree; 
        if (GetAsyncKeyState(VK_UP) == -32767) {
            PageStat = MainMenuThreePosition;
            break;
        }
        // Если нажали вниз, то переходим на страницу PageMainMenuTwo; 
        if (GetAsyncKeyState(VK_DOWN) == -32767) {
            PageStat = MainMenuTwoPosition;
            break;
        }

    }
}

void PageMainMenuTwo(PageStatus& PageStat) {
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Description
    */
    // Начало кнопки Description, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 15;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 241;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "################ Description ################";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,15,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Вторая кнопка Graph
    */
    ColumnSymbolsButton = 9;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 31;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "########## Graph ##########";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,9,15,11 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Третья кнопка Settings
    */
    ColumnSymbolsButton = 12;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 241;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "############# Settings #############";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,14,12,16 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);//  
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  \/
    // Если на данный момент работает эта функция, то StatPageProgram = MainMenuTwoPosition = 3;
    // Тут мы устанавливаем статус переменной PageStat.
    while (true) {
        // Если нажали enter или стрелку вправо, то переходим на страницу Graph 
        if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {
            PageStat = StatPageGraph;
            break;
             // Переход на страницу  Description.
        }
        // Если нажали ESCAPE или стрелку влево, то выходим из приложения
        if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
            PageStat = ProgramExit;
            break;
        }
        // Если нажали вверх, то переходим на страницу PageMainMenuOne; 
        if (GetAsyncKeyState(VK_UP) == -32767) {
            PageStat = MainMenuOnePosition;
            break;
        }
        // Если нажали вниз, то переходим на страницу PageMainMenuThree; 
        if (GetAsyncKeyState(VK_DOWN) == -32767) {
            PageStat = MainMenuThreePosition;
            break;
        }

    }
    

}

void PageMainMenuThree(PageStatus& PageStat) {
    system("cls");
    // Получаю дескриптор для консоли
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD CursorPosition;
    cout << " Title Grapf [ver: 1.02 ]\n";
    cout << " (c) 2021 Global Corporation. All rights reserved.";

    /*
    Первая кнопка Description
    */
    // Начало кнопки Description, c координатами 0,4.
    // Размер кнопки 15,3 идет третъим аргументом в WriteConsoleOutput
    SHORT ColumnSymbolsButton = 15;
    SHORT StringSymbolsButton = 3;
    COORD ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    COORD ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    CHAR_INFO Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    WORD AttributeTap_BufferInfo = 241;
    // 31 Это => FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE;
    string temp = "################ Description ################";
    char StandardButton[50];
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    SMALL_RECT writeArea = { 1,4,15,6 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Вторая кнопка Graph
    */
    ColumnSymbolsButton = 9;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 241;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "########## Graph ##########";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,9,15,11 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    /*
    Третья кнопка Settings
    */
    ColumnSymbolsButton = 12;
    StringSymbolsButton = 3;
    ButtonSize = { ColumnSymbolsButton,StringSymbolsButton };
    // Начальная позиция буфера нашей кнопки 0,0 идет четвертым аргументом в WriteConsoleOutput
    // ButtonBeginPosition = { 0, 0 };
    // Буфет где хранится наша кнопка в последовательном виде размером 45 символов
    // Размер буфера для Description 45 символов
    // Tap_BufferInfo[45];// 45 это => ColumnSymbolsButton* StringSymbolsButton
    // Атрибут цвета нашей кнопки.
    AttributeTap_BufferInfo = 31;
    // 241 Это => FOREGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
    // Синие символы на белом фоне.
    temp = "############# Settings #############";
    strcpy_s(StandardButton, temp.c_str());
    for (int i = 0;i < ColumnSymbolsButton * StringSymbolsButton;i++) {
        Tap_BufferInfo[i].Char.UnicodeChar = StandardButton[i];
        Tap_BufferInfo[i].Attributes = AttributeTap_BufferInfo;
    }
    // Последний аргумент где мы указываем верхнюю левую координату и правую нижнюю координаты для вывода нашего буфера.
    writeArea = { 1,14,12,16 };
    WriteConsoleOutput(ConsoleDescriptor, Tap_BufferInfo, ButtonSize, ButtonBeginPosition, &writeArea);

    // Отрисовка кнопок.
    CursorPosition = { 49,24 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(47) << char(92);
    CursorPosition = { 49,25 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "UP";
    CursorPosition = { 9,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " < BACK";
    CursorPosition = { 84,26 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << " NEXT >";
    CursorPosition = { 48,27 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << "DOWN";
    CursorPosition = { 49,28 };
    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
    cout << char(92) << char(47);//  \/

    // Если на данный момент работает эта функция, то StatPageProgram = MainMenuThreePosition = 4;
    // Тут мы устанавливаем статус переменной PageStat.
    while (true) {
        // Если нажали enter или стрелку вправо, то переходим на страницу Settings 
        if (GetAsyncKeyState(VK_RETURN) == -32767 || GetAsyncKeyState(VK_RIGHT) == -32767) {
            PageStat = StatPageSettings;
            break;
             // Переход на страницу  Settings.
        }
        // Если нажали ESCAPE или стрелку влево, то выходим из приложения
        if (GetAsyncKeyState(VK_ESCAPE) == -32767 || GetAsyncKeyState(VK_LEFT) == -32767) {
            PageStat = ProgramExit;
            break;
        }
        // Если нажали вверх, то переходим на страницу PageMainMenuTwo; 
        if (GetAsyncKeyState(VK_UP) == -32767) {
            PageStat = MainMenuTwoPosition;
            break;
        }
        // Если нажали вниз, то переходим на страницу PageMainMenuOne; 
        if (GetAsyncKeyState(VK_DOWN) == -32767) {
            PageStat = MainMenuOnePosition;
            break;
        }

    }
}



// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
