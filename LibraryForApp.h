#include <iostream>
#include <Windows.h>
#include <string>

// ������������� ��� ��� �������� ������ �������� ���� � �������. 
enum colors_enum
{
    Black = 0,
    Grey = FOREGROUND_INTENSITY,
    LightGrey = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
    White = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    Blue = FOREGROUND_BLUE,
    Green = FOREGROUND_GREEN,
    Cyan = FOREGROUND_GREEN | FOREGROUND_BLUE,
    Red = FOREGROUND_RED,
    Purple = FOREGROUND_RED | FOREGROUND_BLUE,
    LightBlue = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    LightGreen = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
    LightCyan = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    LightRed = FOREGROUND_RED | FOREGROUND_INTENSITY,
    LightPurple = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    Orange = FOREGROUND_RED | FOREGROUND_GREEN,
    Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
    LightRedWithWhiteCLS = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_INTENSITY,
    RedWithWhiteCLS = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_RED,
    LightBlueWithWhiteCLS = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
    BlueWithWhiteCLS = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE,
};


// ������������� ��� ��� �������� ������� ��������� ������� � ��������� ������.
enum PageStatus {
    ProgramStart = 0,
    QProgramStartAnimation = 1,
    MainMenuOnePosition = 2,
    MainMenuTwoPosition = 3,
    MainMenuThreePosition = 4,
    StatPageDescription = 5, // ����� ��������� ��������� �� �������� Description
    StatPageGraph = 6, // ����� ��������� ��������� �� �������� Graph
    StatPageSettings = 7, // ����� ��������� ��������� �� �������� Settings

    ProgramExit = 356, // ������������� ����� ������� VK_ESCAPE ��� VK_LEFT � ������� ����.
};

//void PageMainMenu_1(PageStatus& PageStat) {
//    system("cls");
//    // ������� ���������� ��� �������
//    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);
//    COORD CursorPosition;
//    cout << " Title Grapf [ver: 1.02 ]\n";
//    cout << " (c) 2021 Global Corporation. All rights reserved.";
//    CursorPosition = { 0,4 };
//    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
//    cout << " ###############\n";
//    cout << " # Description #\n";
//    cout << " ###############\n";
//
//    CursorPosition = { 0,9 };
//    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
//    cout << " #########\n";
//    cout << " # Graph #\n";
//    cout << " #########\n";
//    CursorPosition = { 0,14 };
//    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
//    cout << " ############\n";
//    cout << " # Settings #\n";
//    cout << " ############\n";
//    CursorPosition = { 9,26 };
//    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
//    cout << "<| Back"; 
//    CursorPosition = { 84,26 };
//    SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
//    cout << "Next |>";
//    while (1) {
//        if (GetAsyncKeyState(VK_RETURN) == -32767) exit(0);
//    }
//}



void StartAnimation(PageStatus& PageStat) {

    PageStat = QProgramStartAnimation;
    using namespace std;
    // ������� ���������� ��� press enter ��������.
    COORD CursorPosition = { 37,19 };
    // ������� ���������� ��� �������
    HANDLE ConsoleDescriptor = GetStdHandle(STD_OUTPUT_HANDLE);

    cout << "\n\n\n\n\n\n\n\n\n\n\n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n\n";
    cout << "\t    6W'U*L                                                                              \n";
    cout << "\t   8M$#>@$8                                                                             \n";
    cout << "\t   YA.$@<&@                                                                             \n";
    cout << "\t    `YbmY'                                                                              \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t    6W'U*L                                                                              \n";
    cout << "\t   8M$#>@$8                                                                             \n";
    cout << "\t   YA.$@<&@                                                                             \n";
    cout << "\t    `YbmY'     ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t    6W'U*L                                                                              \n";
    cout << "\t   8M$#>@$8                                                                             \n";
    cout << "\t   YA.$@<&@    ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g`YbmY'   `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'8���bgd    MM             MM                    MM                                  \n";
    cout << "\tdM'      `M    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM        `    MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n\n";
    cout << "\t      6W'U*L                                                                            \n";
    cout << "\t     8M$#>@$8                                                                           \n";
    cout << "\t     YA.$@<&@                                                                           \n";
    cout << "\t      `YbmY'                                                                            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t        6W'U*L                                                                          \n";
    cout << "\t       8M$#>@$8                                                                         \n";
    cout << "\t       YA.$@<&@                                                                         \n";
    cout << "\t        `YbmY'                                                                          \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    // 13 � 6n
    cout << "\n\n\n\n\n\n";
    cout << "\t          6W'U*L                                                                        \n";
    cout << "\t         8M$#>@$8                                                                       \n";
    cout << "\t         YA.$@<&@                                                                       \n";
    cout << "\t          `YbmY'                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t            6W'U*L                                                                      \n";
    cout << "\t           8M$#>@$8                                                                     \n";
    cout << "\t           YA.$@<&@                                                                     \n";
    cout << "\t            `YbmY'                                                                      \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t              6W'U*L                                                                    \n";
    cout << "\t             8M$#>@$8                                                                   \n";
    cout << "\t             YA.$@<&@                                                                   \n";
    cout << "\t              `YbmY'                                                                    \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t               6W'U*L                                                                   \n";
    cout << "\t              8M$#>@$8                                                                  \n";
    cout << "\t              YA.$@<&@                                                                  \n";
    cout << "\t               `YbmY'                                                                   \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                 6W'U*L                                                                 \n";
    cout << "\t                8M$#>@$8                                                                \n";
    cout << "\t                YA.$@<&@                                                                \n";
    cout << "\t                 `YbmY'                                                                 \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                   6W'U*L                                                               \n";
    cout << "\t                  8M$#>@$8                                                              \n";
    cout << "\t                  YA.$@<&@                                                              \n";
    cout << "\t                   `YbmY'                                                               \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                     6W'U*L                                                             \n";
    cout << "\t                    8M$#>@$8                                                            \n";
    cout << "\t                    YA.$@<&@                                                            \n";
    cout << "\t                     `YbmY'                                                             \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                       6W'U*L                                                           \n";
    cout << "\t                      8M$#>@$8                                                          \n";
    cout << "\t                      YA.$@<&@                                                          \n";
    cout << "\t                       `YbmY'                                                           \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                         6W'U*L                                                         \n";
    cout << "\t                        8M$#>@$8                                                        \n";
    cout << "\t                        YA.$@<&@                                                        \n";
    cout << "\t                         `YbmY'                                                         \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                           6W'U*L                                                       \n";
    cout << "\t                          8M$#>@$8                                                      \n";
    cout << "\t                          YA.$@<&@                                                      \n";
    cout << "\t                           `YbmY'                                                       \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                             6W'U*L                                                     \n";
    cout << "\t                            8M$#>@$8                                                    \n";
    cout << "\t                            YA.$@<&@                                                    \n";
    cout << "\t                             `YbmY'                                                     \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                               6W'U*L                                                   \n";
    cout << "\t                              8M$#>@$8                                                  \n";
    cout << "\t                              YA.$@<&@                                                  \n";
    cout << "\t                               `YbmY'                                                   \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                 6W'U*L                                                 \n";
    cout << "\t                                8M$#>@$8                                                \n";
    cout << "\t                                YA.$@<&@                                                \n";
    cout << "\t                                 `YbmY'                                                 \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                 6W'U*L                                                 \n";
    cout << "\t                                8M$#>@$8                                                \n";
    cout << "\t                                YA.$ @<&@                                               \n";
    cout << "\t                                 `Yb  mY'                                               \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                   6W'U*L                                               \n";
    cout << "\t                                  8M$  #>@$8                                            \n";
    cout << "\t                                 YA.$  @<&@                                             \n";
    cout << "\t                                 `Yb    mY'                                             \n";
    cout << "\t                                                                                        \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                    6W'U*L                                              \n";
    cout << "\t                                  8M$    #>@$8                                          \n";
    cout << "\t                                 YA.$    @<&@                                           \n";
    cout << "\t                                 `Yb     mY'                                            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                 8M$6W'U*L#>@$8                                         \n";
    cout << "\t                                YA.$       @<&@                                         \n";
    cout << "\t                              `Yb           mY'                                         \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                 8M$6W'U*L#>@$8                                         \n";
    cout << "\t                              YA.$           @<&@                                       \n";
    cout << "\t                           `Yb                 mY'                                      \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                               8M $6W 'U*L #>@$8                                        \n";
    cout << "\t                          YA.$                   @<&@                                   \n";
    cout << "\t                       `Yb                            mY'                               \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                        YA.$  8M  $6W  'U*L  #>@$8  @< &@                               \n";
    cout << "\t                  `Yb                                      mY'                          \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t             `Yb     YA.$    8M     $6W     'U*L    #>@$8    @<   &@     mY'            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t                                                                                        \n";
    cout << "\t           !&@   !&@     !&@    !&@    !&@     !&@       !&@    !&@  !&@                \n";
    cout << "\t             !&@     !&@$    !&@    !&@     !&@L    #>@$8    !&@  !&@    !&@            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";


    Sleep(100);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t            ___                                                _                        \n";
    cout << "\t           | _ R    _ _     ___     ___     ___      _ _      | |_       ___            \n";
    cout << "\t           |  _/   | '_|   / -_)   (_-<    / -_)    | ' R     |  _|     (_-<            \n";
    cout << "\t           |_|     |_|     R___|   /__/    R___|    |_||_|     R__|     /__/            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";

    Sleep(1000);
    system("cls");

    cout << "\n\n\n\n\n\n";
    cout << "\t                                                                                        \n";
    cout << "\t            ___                                                _                        \n";
    cout << "\t           | _ R    _ _     ___     ___     ___      _ _      | |_       ___            \n";
    cout << "\t           |  _/   | '_|   / -_)   (_-<    / -_)    | ' R     |  _|     (_-<            \n";
    cout << "\t           |_|     |_|     R___|   /__/    R___|    |_||_|     R__|     /__/            \n";
    cout << "\t               ,,             ,,                    ,,        ,,                        \n";
    cout << "\t  .g8���bgd  `7MM            *MM                  `7MM        db                        \n";
    cout << "\t.dP'     `M    MM             MM                    MM                                  \n";
    cout << "\tdM'       `    MM   ,pW�Wq.   MM,dMMb.    ,6�Yb.    MM      `7MM  `7MMpMMMb.   ,p6�bo   \n";
    cout << "\tMM             MM  6W'   `Wb  MM    `Mb  8)   MM    MM        MM    MM    MM  6M'  OO   \n";
    cout << "\tMM.    `7MMF'  MM  8M     M8  MM     M8   ,pm9MM    MM        MM    MM    MM  8M        \n";
    cout << "\t`Mb.     MM    MM  YA.   ,A9  MM.   ,M9  8M   MM    MM   ,,   MM    MM    MM  YM.    ,  \n";
    cout << "\t  `�bmmmdPY  .JMML. `Ybmd9'   P^YbmdP'   `Moo9^Yo..JMML .db .JMML..JMML  JMML. YMbmd'   \n";
    cout << "\t                             Press Enter to Continue                                    \n";

    DWORD StartTime = 0;
    bool FlagSwitchEmptyOrEnter = TRUE;

    // ������ ���� ��� ������������� ��������� �������, ��� ������� ��� ����, ��� ������� �� ����.
    // ��� ��������� ��������� ������� ENTER. 
    while (1) {
        if (StartTime == 0) StartTime = GetTickCount() + 500;
        if (GetTickCount() > StartTime) {
            SetConsoleCursorPosition(ConsoleDescriptor, CursorPosition);
            if (FlagSwitchEmptyOrEnter) {
                cout << "                       ";
                StartTime = 0;
                FlagSwitchEmptyOrEnter = FALSE;
            }
            else {
                cout << "Press Enter to Continue";
                StartTime = 0;
                FlagSwitchEmptyOrEnter = TRUE;
            }
        }
        // ���� ������ Enter
        if (GetAsyncKeyState(VK_RETURN) == -32767)break;
    }
    PageStat = MainMenuOnePosition;
    system("cls");
    // ����� ������� �����������, ������� �� ��������� ��������. 
}